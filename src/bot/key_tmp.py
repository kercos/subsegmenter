NGROK = False 
# True if you want to test on local host via ngrok
# Don't forget to set webhook (python -m bot.set_webhook)

APP_NAME = 'your_app_name'
BOT_USERNAME = 'your_bot_username'

if NGROK:
    from bot import ngrok
    APP_BASE_URL = ngrok.get_ngrok_base()
else:
    APP_BASE_URL = 'https://{}.appspot.com'.format(APP_NAME)

WEBHOOK_TELEGRAM_ROUTING = '/webhook/some_hidden_routing'
WEBHOOK_TELEGRAM_BASE = APP_BASE_URL + WEBHOOK_TELEGRAM_ROUTING

TELEGRAM_API_TOKEN = 'your_telegram_api_token'
TELEGRAM_API_URL = 'https://api.telegram.org/bot{}/'.format(TELEGRAM_API_TOKEN)
TELEGRAM_API_URL_FILE = 'https://api.telegram.org/file/bot{}/'.format(TELEGRAM_API_TOKEN)

TELEGRAM_BOT_MASTER_ID = 'your_telegram_id' # check @myidbot 

AMARA_HEADERS = {
    'X-api-username': 'your_amara_username', 
    'X-api-key':'your_amara_api_key'
}
