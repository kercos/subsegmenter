import telegram

from bot import key
from bot import bot_ui
from bot.bot_ndb_user import NDB_User
from subsegmenter import parameters

import logging
import traceback

BOT = telegram.Bot(token=key.TELEGRAM_API_TOKEN)

DISABLED = False

def exception_reporter(func, *args, **kwargs):    
    def dec(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception:
            report_string = '❗️ Exception {}'.format(traceback.format_exc()) #.splitlines()
            logging.error(report_string)          
            try:  
                report_master(report_string)
            except Exception:
                report_string = '❗️ Exception {}'.format(traceback.format_exc())
                logging.error(report_string)          
    return dec

def run_new_thread_and_report_exception(func, *args, **kwargs):
    import threading   

    @exception_reporter
    def report_exception_in_thread(func, *args, **kwargs):
        func(*args,  **kwargs)

    args= list(args)
    args.insert(0, func)
    threading.Thread(target=report_exception_in_thread, args=args, kwargs=kwargs).start()
    

def set_webhook():
    s = BOT.setWebhook(key.WEBHOOK_TELEGRAM_BASE, allowed_updates=['message'])
    if s:
        print("webhook setup ok: {}".format(key.WEBHOOK_TELEGRAM_BASE))
    else:
        print("webhook setup failed")

def delete_webhook():
    BOT.deleteWebhook()

def get_webhook_info():
    print(BOT.get_webhook_info())

'''
python-telegram-bot documentation
https://python-telegram-bot.readthedocs.io/en/stable/
'''

@exception_reporter
def deal_with_request(request_json):
    # retrieve the message in JSON and then transform it to Telegram object
    update_obj = telegram.Update.de_json(request_json, BOT)
    message_obj = update_obj.message    
    user_obj = message_obj.from_user
    name = user_obj.first_name
    if user_obj.last_name:
        name += ' ' + user_obj.last_name
    user = NDB_User('telegram', user_obj.id, name, user_obj.username)
    
    if message_obj.text:
        deal_with_text_request(user, message_obj.text)
    elif message_obj.photo:
        deal_with_photo_request(user, message_obj.photo)
    elif message_obj.document:
        deal_with_document_request(user, message_obj.document)
    else:
        logging.debug('TELEGRAM: no text of photo in request')
        return


DEFAULT_KEYBOARD = [[bot_ui.BUTTON_INFO]]
DEFAULT_REPLY_MARKUP = telegram.ReplyKeyboardMarkup(DEFAULT_KEYBOARD, resize_keyboard=True)

def send_message(user, text, markdown=True):
    if markdown:
        BOT.sendMessage(
            chat_id = user.serial_number, 
            text = text,
            parse_mode = telegram.ParseMode.MARKDOWN,
            reply_markup = DEFAULT_REPLY_MARKUP
        )
    else:
        BOT.sendMessage(
            chat_id = user.serial_number,          
            text = text,
            reply_markup = DEFAULT_REPLY_MARKUP
        )

def send_text_document(user, file_name, file_content):
    import requests
    files = [('document', (file_name, file_content, 'text/plain'))]
    data = {'chat_id': user.serial_number,}
    resp = requests.post(key.TELEGRAM_API_URL + 'sendDocument', data=data, files=files)
    logging.debug("Sent documnet. Response status code: {}".format(resp.status_code))

def deal_with_text_request(user, text):
    from subsegmenter import segmenter
    #from subsegmenter.penalizer_basic import Penalizer_basic
    from subsegmenter.penalizer_dep import Penalizer_dep
    if text in ['/start', '/help', bot_ui.BUTTON_INFO] or text.lower() in bot_ui.INFO_QUESTIONS_LOWER:
        reply_text = bot_ui.intro()
        send_message(user, reply_text)    
        return
    if text == '/exception':
        1/0
        return
    if text.startswith('/debug') and ' ' in text:
        on_off = text.lower().split()[1]
        if on_off in ['on','off']:
            user.debug = on_off == 'on'
            user.put()
            msg = "Debug attivato" if user.debug else "Debug disattivato"
            send_message(user, msg)    
            return     
    if text.startswith('/'):
        send_message(user, bot_ui.wrong_input(text))    
        return    
    if len(text) > parameters.MAX_CHAR_FOR_TEXT_SEGMENTATION:
        response_msg = "❗️ Il testo inserito è troppo lungo."
        send_message(user, response_msg)    
    else:
        penalizer = Penalizer_dep()   
        segmented_text = segmenter.segment_text(penalizer, text)
        response_msg = "*Segmentation:*\n{}".format(segmented_text)
        send_message(user, response_msg)    
    logging.debug('TELEGRAM Reply to message from {} with text {} -> {}'.format(user.serial_number, text, response_msg))            

def get_url_from_file_id(file_id):    
    import requests
    logging.debug("TELEGRAM: Requested file_id: {}".format(file_id))
    r = requests.post(key.TELEGRAM_API_URL + 'getFile', data={'file_id': file_id})
    r_json = r.json()
    if 'result' not in r_json or 'file_path' not in r_json['result']:
        logging.warning('No result found when requesting file_id: {}'.format(file_id))
        return None
    file_url = r_json['result']['file_path']
    return file_url

def get_raw_content_from_file(file_id):
    import requests
    file_url_suffix = get_url_from_file_id(file_id)
    file_url = key.TELEGRAM_API_URL_FILE + file_url_suffix
    r = requests.get(file_url)
    return r.content

def get_content_from_file(file_id):
    import requests
    file_url_suffix = get_url_from_file_id(file_id)
    file_url = key.TELEGRAM_API_URL_FILE + file_url_suffix
    r = requests.get(file_url)
    return r.content

def deal_with_photo_request(user, photo_list):
    reply_text = 'Photo input'
    send_message(user, reply_text)    

def deal_with_document_request(user, document_obj):          
    if DISABLED:
        reply_text = "Temporary disabled"
        send_message(user, reply_text, markdown=False)    
        return
    file_id = document_obj.file_id
    file_name = document_obj.file_name    
    mime_type = document_obj.mime_type
    file_size = document_obj.file_size #bytes    
    logging.debug('Receiving document: {}'.format(file_name))
    logging.debug("File size: {}".format(file_size))
    if file_size > 102400:
        reply_text = "File too big."
        send_message(user, reply_text)    
    elif file_name.endswith('.zip') or (mime_type and mime_type.endswith('.zip')): #"application/zip":
        run_new_thread_and_report_exception(segment_zip, user, file_id, file_name)
    else:    
        run_new_thread_and_report_exception(segment_file, user, file_id, file_name)

def segment_zip(user, file_id, file_name):
    import zipfile
    from subsegmenter import segmenter
    import io
    import os
    import codecs  
    reply_text = "Batch segmentation of `{}`, please wait ...".format(file_name)    
    send_message(user, reply_text, markdown=False)    
    penalizer = parameters.DEFAULT_PENALIZER
    src_zip_buffer = io.BytesIO(get_content_from_file(file_id))             
    dst_zip_buffer = io.BytesIO()
    zf = zipfile.ZipFile(dst_zip_buffer, "a", zipfile.ZIP_DEFLATED, False)        
    num_file, num_segmented = 0, 0
    with zipfile.ZipFile(src_zip_buffer, "r") as input_zip:
        for name in input_zip.namelist():
            if input_zip.getinfo(name).is_dir():
                continue
            basename = os.path.basename(name)
            if basename.startswith('.'):
                continue
            num_file += 1
            subtile_content = input_zip.read(name)            
            try:
                subtile_text = codecs.decode(subtile_content, 'utf-8')
            except UnicodeDecodeError:
                error_msg = "🤯 File {} is not encoded as utf-8.".format(basename)
                send_message(user, error_msg, markdown=False)
                continue
            try:
                segmented_text = segmenter.segment_text(penalizer, subtile_text)
            except segmenter.TokenExceedsMaxCharsPerLineError:
                error_msg = "🤯 File {} contains a word that exceeds the max number of chars allowed in a subtitle line.".format(basename)
                send_message(user, error_msg, markdown=False)
                continue 
            except Exception as e:
                error_msg = "🤯 Encountered problem to segment file {}. Please contact @kercos.".format(basename)
                send_message(user, error_msg, markdown=False)
                return 
            num_segmented += 1
            converted_msg = "📄 File segmented {}".format(basename)
            send_message(user, converted_msg)
            new_file_name_in_zip = name.split('.')[0] + '_{}.txt'.format(penalizer.VERSION)
            zf.writestr(new_file_name_in_zip, segmented_text)
    zf.close()
    dst_zip_content = dst_zip_buffer.getvalue() #.read()
    new_file_name = file_name.split('.')[0] + '_{}.zip'.format(penalizer.VERSION)
    report_msg = "Segmented file: {}/{}".format(num_segmented, num_file)
    send_message(user, report_msg)
    send_text_document(user, new_file_name, dst_zip_content)      

def segment_file(user, file_id, file_name):
    from subsegmenter import segmenter
    import codecs
    from subsegmenter.utility import check_file_encoding
    penalizer = parameters.DEFAULT_PENALIZER
    reply_text = "Segmentation of `{}`, please wait ...".format(file_name)
    send_message(user, reply_text, markdown=False)    
    new_file_name = file_name.split('.')[0] + '_{}.txt'.format(penalizer.VERSION)
    subtile_content = get_raw_content_from_file(file_id)             
    try:
        subtile_text = codecs.decode(subtile_content, 'utf-8')
    except UnicodeDecodeError:
        error_msg = "🤯 The file is not encoded as utf-8."
        send_message(user, error_msg, markdown=False)
        return     
    try:
        segmented_text = segmenter.segment_text(penalizer, subtile_text)
    except segmenter.TokenExceedsMaxCharsPerLineError:
        error_msg = "🤯 The file contains a word that exceeds the max number of chars allowed in a subtitle line."
        send_message(user, error_msg, markdown=False)
        return 
    except Exception as e:
        error_msg = "🤯 Encountered problem to segment file {}. Please contact @kercos."
        send_message(user, error_msg, markdown=False)
        return 
    send_text_document(user, new_file_name, segmented_text)        

bot_telegram_MASTER = None

def report_master(message):
    global bot_telegram_MASTER
    if bot_telegram_MASTER is None:
        bot_telegram_MASTER = NDB_User('telegram', key.TELEGRAM_BOT_MASTER_ID, update=False)
    send_message(bot_telegram_MASTER, message, markdown=False)    
