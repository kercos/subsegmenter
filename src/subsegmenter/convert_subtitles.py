import json
from subsegmenter import parameters
import re

def extract_json_blocks_from_json_subtitles(input_subtitle):
    with open(input_subtitle) as f_in:
        sub_json = json.load(f_in)
        sub_list = sub_json['subtitles']
        for json_block in sub_list:
            yield json_block

TAG_RE = re.compile(r'<(.+)>(.+)</\1>')

def get_lines_from_block(text):
    lines = [l.strip() for l in text.split('<br>') if l]
    lines = [TAG_RE.sub(r'\2',l) for l in lines] # remove tags in lines
    return lines

def extract_blocks_from_json_subtitle(input_subtitle):
    for json_block in extract_json_blocks_from_json_subtitles(input_subtitle):        
        sub_tex = json_block['text']
        lines = get_lines_from_block(sub_tex)
        yield lines

def check_correctness(input_subtitle):
    for json_block in extract_json_blocks_from_json_subtitles(input_subtitle):
        start = json_block['start']
        end = json_block['end']
        sub_tex = json_block['text']
        lines = get_lines_from_block(sub_tex)
        if len(lines) > parameters.MAX_LINES_PER_SUBTITLES:
            return False, 'More than two lines per block ({})'.format(lines[0])
        for line in lines:
            if len(line) > parameters.MAX_CHARS_PER_LINE:
                error_msg = 'Line with more than {} characters: "{}" ({})'.format(parameters.MAX_CHARS_PER_LINE, line, len(line))
                return False, error_msg
        if start and end:
            tot_char_length = float(sum(1 for l in lines for c in l if c not in [' ','\n'] ))
            tot_sec = float(end-start)/1000
            if tot_sec==0:
                error_msg = 'Tot sec is 0: {}'.format(line)
                return False, error_msg
            char_per_sec = tot_char_length/tot_sec
            if char_per_sec > parameters.MAX_CHARS_PER_SECOND:
                error_msg = 'More than {} chars per sec in line: "{}" ({})'.format(parameters.MAX_CHARS_PER_SECOND, line, char_per_sec)
                return False, error_msg
    return True, None


def json_to_plain(input_subtitle, output_file):
    with open(output_file, 'w') as f_out:
        for block in extract_blocks_from_json_subtitle(input_subtitle):
            f_out.write('\n'.join(block))
            f_out.write('\n\n')

def plain_to_flat(input_plain, output_flat):
    with open(output_flat, 'w') as f_out:
        with open(input_plain) as f_in:
            lines = [l.strip() for l in f_in.readlines()]
            flat_text = ' '.join(lines)
            flat_text = flat_text.replace('  ', ' ')
            f_out.write(flat_text)

def json_to_plain_multi(lang):
    import os
    input_dir_path = os.path.join('..','subtitles', 'amara', 'json', lang)
    output_dir_path = os.path.join('..','subtitles', 'amara', 'plain', lang)
    files = os.listdir(input_dir_path)
    for f in files:
        if not f.endswith('.json'):
            continue   
        input_file = os.path.join(input_dir_path, f)        
        output_file = os.path.join(output_dir_path, f[:-5] + ".txt")
        print('Converting {} -> {}'.format(input_file, output_file))        
        correct, error_msg = check_correctness(input_file)
        if not correct:
            print('\t' + error_msg)
        json_to_plain(input_file, output_file)    

def plain_to_flat_multi(lang):
    import os
    input_dir_path = os.path.join('..','subtitles', 'amara', 'plain', lang)
    output_dir_path = os.path.join('..','subtitles', 'amara', 'flat', lang)
    files = os.listdir(input_dir_path)
    for f in files:
        if not f.endswith('.txt'):
            continue   
        input_file = os.path.join(input_dir_path, f)        
        output_file = os.path.join(output_dir_path, f[:-5] + ".txt")
        print('Converting {} -> {}'.format(input_file, output_file))        
        plain_to_flat(input_file, output_file)    


if __name__ == "__main__":
    for lang in ['en']: 
        json_to_plain_multi(lang)
        plain_to_flat_multi(lang)
