from subsegmenter import parameters
import regex as re

p = re.compile(r'(?<=[.,:;?!)\]]+)(?=[^\s\d])')

def add_space_after_punctuation(text):
    return p.sub(r' ', text)    

def split_text(text):
    text = add_space_after_punctuation(text)
    from sentence_splitter import SentenceSplitter
    splitter = SentenceSplitter(language='it')
    return splitter.split(text)

# def split_text(text):    
#     import spacy
#     text = add_space_after_punctuation(text)
#     nlp = spacy.load('it_core_news_sm')
#     doc = nlp(text)
#     sentences = [sent.text for sent in doc.sents]
#     return sentences

def tokenize_text(text):
    return text.split()

def split_tokenize_text(text):
    return [tokenize_text(sentence) for sentence in split_text(text)]    


if __name__ == "__main__":
    #text = "Il Dr. Rossi è andato a Roma. Nel mondo ci sono 22 milioni di consumatori di cocaina. Di questi, 9 milioni sono distribuiti nelle Americhe e 5 milioni in Europa. Perché la cocaina, che sembra continui ad espandersi senza sosta, ha questo grande appeal nei confronti degli abitanti del nostro pianeta? La risposta è abbastanza semplice. Perché produce un grande piacere. Un piacere che è tremendamente simile a quello del primo amore e quindi un piacere che tutti ricordiamo e che tutti vorremmo riprovare come nel momento in cui lo abbiamo provato la prima volta. Ma se fosse per questo motivo, la cocaina sarebbe sì un problema ma relativamente."
    text = "Come prima cosa, salve! Mi chiamo Dunja Burghardt e se il Dr. Dueck è stato il culmine di questa manifestazione."    
    print(split_text(text))
    # print(tokenize_text(text))
    # print(split_tokenize_text(text))