from subsegmenter import segmenter
from subsegmenter.penalizer_basic import Penalizer_basic
from subsegmenter.penalizer_dep import Penalizer_dep


def test_line(penalizer):
    #       0  1  2     3   4      5       6  7       8       9   10 11       12 13
    # line = "Ma se fosse per questo motivo, la cocaina sarebbe si' un problema ma relativamente."
    line = "Nel mondo ci sono 22 milioni di consumatori di cocaina. Di questi, 9 milioni sono distribuiti nelle Americhe e 5 milioni in Europa. Perché la cocaina, che sembra continui ad espandersi senza sosta, ha questo grande appeal nei confronti degli abitanti del nostro pianeta? La risposta è abbastanza semplice. Perché produce un grande piacere. Un piacere che è tremendamente simile a quello del primo amore e quindi un piacere che tutti ricordiamo e che tutti vorremmo riprovare come nel momento in cui lo abbiamo provato la prima volta. Ma se fosse per questo motivo, la cocaina sarebbe sì un problema ma relativamente."
    print(segmenter.segment_text(penalizer, line, debug=False))

def test_amara():
    import os
    penalizer = Penalizer_dep()
    files = os.listdir('../subtitles/amara/flat')
    for f in files:
        if not f.endswith('.txt'):
            continue   
        input_file = os.path.join('../subtitles/amara/flat', f)        
        output_file = os.path.join('../subtitles/amara/depV1', f[:-5] + "{}.txt".format(penalizer.VERSION))
        print('Segmenting {} -> {}'.format(input_file, output_file))        
        segmenter.segment_file(penalizer, input_file, output_file)

if __name__ == "__main__":   
    
    # basic_penalizer = Penalizer_basic() 
    # dep_penalizer = Penalizer_dep()
    #test_line()
    #segment_file(dep_penalizer, subtitles.test_subtitle_flat, subtitles.test_subtitle_segmented_auto)
    #segment_file(dep_penalizer, subtitles.cescati_subtitle_flat, subtitles.cescati_subtitle_segmented_auto)
    #segment_dir('/Users/fedja/Downloads/SbobinatureDaPassareAlBot')
    #segment_file(basic_penalizer, '../subtitles/gallimberti_plain.txt','../subtitles/gallimberti_{}.txt'.format(penalizer.VERSION))
    #segment_file(dep_penalizer, '../subtitles/gallimberti_plain.txt','../subtitles/gallimberti_{}.txt'.format(penalizer.VERSION))
    
    #segmenter.segment_file(dep_penalizer, '../subtitles/amara/flat/en_SR5JeI1hpEV.txt','../subtitles/amara/depV1/en_SR5JeI1hpE_{}.txt'.format(penalizer.VERSION)
    
    #test_amara()

    from bot_ndb_user import NDB_User
    from bot_telegram import segment_zip
    user = NDB_User.get_admin()
    segment_zip(user, 'BQADBAADywQAAmSzaFBa8Y1SNdfd7AI', 'archive.zip')
