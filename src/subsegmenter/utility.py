
def guess_encoding(rawdata):
    import chardet
    confidence_encoding_scores = chardet.detect(rawdata)    
    if confidence_encoding_scores:
        return confidence_encoding_scores['encoding']
    raise UnicodeDecodeError

def check_encoding(rawdata, encoding='utf-8'):
    import codecs
    try:
        codecs.decode(rawdata, encoding)
    except UnicodeDecodeError:
        return False
    return True

def check_file_encoding(filename, encoding='utf-8'):
    import codecs
    try:
        f = codecs.open(filename, encoding=encoding, errors='strict')
        for line in f:
            pass
        return True
    except UnicodeDecodeError:
        return False

def test_exception():
    from subsegmenter import TokenExceedsMaxCharsPerLineError
    try:
        raise ValueError()
    except TokenExceedsMaxCharsPerLineError as e:
        print('Single token exceeds max characters per line.')        
    except Exception as e:
        print(e)


def test():
    import zipfile
    import io
    import os
    sub_path = '/Users/fedja/Downloads/SubTest'
    zip_file = '/Users/fedja/Downloads/SubTest.zip'
    from os.path import isfile, join
    sub_files = [f for f in os.listdir(sub_path) if isfile(join(sub_path, f))]
    src_zip_buffer = io.BytesIO(open(zip_file, 'rb').read())
    with zipfile.ZipFile(src_zip_buffer, "r") as input_zip:
        for name in input_zip.namelist():
            basename = os.path.basename(name)
            if basename.startswith('.'):
                continue
            print('Reading file {} in zip'.format(basename))
            subtile_content = input_zip.read(name)


if __name__ == "__main__":
    #infile = '/Users/fedja/Downloads/SbobinaturePronte/GiuliaDetomati.txt'
    #rawdata = open(infile, 'rb').read()
    #print(check_encoding(rawdata))    
    # test_exception()
    test()