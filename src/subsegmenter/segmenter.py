from subsegmenter import parameters
from subsegmenter import subtitles
from subsegmenter import tokenizer
from subsegmenter.penalizer_basic import Penalizer_basic
from subsegmenter.penalizer_dep import Penalizer_dep

import logging
from collections import defaultdict


logging.basicConfig(level=logging.DEBUG,
                    format='SEGMENTER_%(levelname)s %(message)s') #%(asctime)s

class TokenExceedsMaxCharsPerLineError(Exception):
    pass

########################
## LINE SEGMENTER
########################
class LineSegmenter:

    def __init__(self, tokens, penalizer):
        self.tokens = tokens
        self.penalizer = penalizer
        self.boundary_penalties = penalizer.get_sequence_penalties(tokens)
        self.best_segmentation_table = {}

    def chop(self, debug = False):    
        penalty, boundaries = self.get_best_segmentation()        
        boundaries = [-1] + list(boundaries) + [len(self.tokens)]
        segmentation = []
        current_subtitle = []
        for i in range(len(boundaries)-1):
            start, end = boundaries[i]+1, boundaries[i+1]+1
            line = ' '.join(self.tokens[start:end])
            current_subtitle.append(line)
            if len(current_subtitle)==2:
                segmentation.append(current_subtitle)
                current_subtitle = []
        if current_subtitle:
            segmentation.append(current_subtitle)
        return penalty, segmentation
        # [ ["sub1_line1", "sub1_line2"], ["sub2_line1"]  ]


    def get_best_segmentation(self):   
        self.best_segmentation_table[len(self.tokens)-1] = 0, []
        for boundary_index in range(len(self.tokens)-2,-2,-1):
            next_boundary_indexes = self.next_boundary_indexes(boundary_index)
            sub_results = {}
            for i in next_boundary_indexes:
                i_sub_penalty, i_sub_segmentation = self.best_segmentation_table[i]
                tokens_in_line = i - boundary_index
                i_sub_penalty += self.penalizer.get_line_length_penalty(tokens_in_line)
                sub_results[i] = i_sub_penalty, i_sub_segmentation
            best_sub_penalty, best_sub_segmentation = sorted(sub_results.items(), key = lambda item:(item[1][0]))[0][1]
            updated_penalty = best_sub_penalty + self.boundary_penalties[boundary_index]
            if boundary_index!=-1:
                updated_sub_segmentation = [boundary_index] + list(best_sub_segmentation)
            else:
                updated_sub_segmentation = list(best_sub_segmentation)
            self.best_segmentation_table[boundary_index] = updated_penalty, updated_sub_segmentation
        return self.best_segmentation_table[-1]

    def next_boundary_indexes(self, boundary_index):              
        first_token = self.tokens[boundary_index+1]
        if len(first_token)>parameters.MAX_CHARS_PER_LINE:
            raise TokenExceedsMaxCharsPerLineError()
        char_count = 0  
        result = []
        for n, t in enumerate(self.tokens[boundary_index+1:], boundary_index+1):
            next_char_count = char_count + len(t)
            if next_char_count > parameters.MAX_CHARS_PER_LINE:
                break
            char_count = next_char_count + 1 # including space
            result.append(n)
        return result

    def debug_segmentation(self):
        import json
        debug_structure = {}        
        for i in range(-1,len(self.tokens)):
            penalty, best_segmentation = self.best_segmentation_table[i]
            value = debug_structure[str(i)] = {                                
                'segmentation': best_segmentation,
                'segmentation penalty': penalty
            }
            if i!=-1:
                value['token'] = self.tokens[i]
                value['boundary penalty'] = self.boundary_penalties[i]        
        debug_structure['OVERALL'] = debug_structure.pop('-1')
        logging.debug(json.dumps(debug_structure, indent=4, sort_keys=True))


########################
## SUBTITLE SEGMENTER
########################
class SubSegmenter:

    def __init__(self, penalizer, input_file_path=None, input_text=None):
        # new lines in input file should be treated as subtitles bounaries
        assert input_file_path!=None or input_text!=None
        self.penalizer = penalizer
        if input_file_path:
            with open(input_file_path) as f_in:
                lines = [l.strip() for l in f_in.readlines() if len(l)>0]                
        else:
            lines = [l.strip() for l in input_text.splitlines() if l.strip()]
        self.sub_tokenized_lines = [tokenizer.split_tokenize_text(line) for line in lines]
        
    def get_tokenized_lines(self):
        return self.sub_tokenized_lines

    def get_max_tokens_in_line(self):
        return max(len(sub_tok_line) for sub_tok_line in self.sub_tokenized_lines) 

    def chop(self, output_path=None, debug=False):        
        subtitles = [] # [ ["sub1_line1", "sub1_line2"], ["sub2_line1"], ["sub3_line1", "sub3_line2"]  ]
        total_penalty = 0
        for sub_tok_line in self.sub_tokenized_lines:
            for tokens in sub_tok_line:
                line_segmenter = LineSegmenter(tokens, self.penalizer)
                penalty, line_subtitles = line_segmenter.chop()
                if debug:
                    line_segmenter.debug_segmentation()
                total_penalty += penalty
                subtitles.extend(line_subtitles)        
        self.subtitles = subtitles
        if debug:
            logging.debug("Total penalty: {}".format(total_penalty))
        if output_path:
            with open(output_path, 'w') as f_out:
                for sub in subtitles:
                    for line in sub:
                        f_out.write(line + '\n')
                    f_out.write('\n')
        else:
            return '\n\n'.join(['\n'.join(s) for s in subtitles])

def segment_text(penalizer, text, debug=False):    
    segmenter = SubSegmenter(penalizer, input_text = text)    
    return segmenter.chop(debug)

def segment_file(penalizer, input_file_path, output_file_path=None, output_writable_func=None):
    import os
    if output_file_path is None:
        output_file_path = input_file_path.split('.')[0] + '_{}.txt'.format(penalizer.VERSION)        
    try:
        segmenter = SubSegmenter(penalizer, input_file_path)        
        segmenter.chop(output_file_path)
    except TokenExceedsMaxCharsPerLineError:
        filebase = os.path.basename(input_file_path)
        if output_writable_func is not None:
            output_writable_func("Il file {} contiene una parola che supera il numero massimo di caratteri consentiti in una riga.\n".format(filebase))
        return False
    except UnicodeDecodeError:
        filebase = os.path.basename(input_file_path)
        if output_writable_func is not None:
            output_writable_func("Il file {} non è in formato utf-8\n".format(filebase))
        return False
    return True

def segment_dir(penalizer, input_dir_path, output_writable_func=None):
    import os
    assert os.path.isdir(input_dir_path)
    file_list = os.listdir(input_dir_path)
    total, segmented = 0, 0
    for file_in_name in file_list:
        if file_in_name.startswith('.'):
            continue
        total += 1
        file_in_path = os.path.join(input_dir_path, file_in_name)
        new_file_name = file_in_path.split('.')[0] + '_{}.txt'.format(penalizer.VERSION)
        file_out_path = os.path.join(input_dir_path, new_file_name)
        if segment_file(penalizer, file_in_path, file_out_path, output_writable_func):
            segmented += 1
            if output_writable_func:
                filebase = os.path.basename(file_in_path)
                output_writable_func('Segmented file: {}\n'.format(filebase))
    return total, segmented