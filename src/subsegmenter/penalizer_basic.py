DET_SET = {'lo', 'la', 'il', 'le', 'i', 'gli', 'un', 'uno', 'una'}

PREP_SET = {'di', 'a', 'ad', 'da', 'in', 'con', 'su', 'per', 'tra', 'fra', 'sopra', 'sotto'}

CONG_SET = {'e', 'ed', 'o'}

PREP_ART_SET = {
    'del', 'dello', 'della', 'dei', 'degli', 'delle', 
    'al', 'allo', 'alla', 'ai', 'agli', 'alle', 
    'dal', 'dallo', 'dalla', 'dai', 'dagli', 'dalle', 
    'nel', 'nello', 'nella', 'nei', 'negli', 'nelle', 
    'col', 'collo', 'colla', 'coi', 'cogli', 'colle', 
    'sul', 'sullo', 'sulla', 'sui', 'sugli', 'sulle'
}

NOT_LONELEY = DET_SET | PREP_SET | PREP_ART_SET | {'che', 'né'}

GOOD_BEGINNING_WORD = {'e', 'ed', 'o', 'oppure', 'invece', 'meno', 'invero', 'quale', 'meglio', 'poiché', 'mentre', 'allora', 'se', 'altresì', 'sebbene', 'appena', 'tranne', 'dopo', 'cioè', 'perciò', 'persino', 'siccome', 'intanto', 'giacché', 'vale', 'dunque', 'eppure', 'difatti', 'fintantoché', 'pure', 'quantunque', 'prima', 'fuorché', 'insomma', 'tanto', 'quindi', 'affinché', 'perché', 'anzi', 'come', 'eccetto', 'nonostante', 'perfino', 'sicché', 'tuttavia', 'più', 'però', 'nemmeno', 'piuttosto', 'altrimenti', 'infatti', 'così', 'neanche', 'neppure', 'quanto', 'ma', 'senza', 'dacché', 'anche', 'inoltre', 'finché', 'purché', 'onde', 'tale', 'qualora', 'acciocché', 'salvo', 'pertanto', 'ogni', 'qualsiasi', 'ovvero', 'nonché', 'ossia', 'peggio', 'cosicché', 'ancora', 'quando', 'benché'}

GOOD_BEGINNING_PUNCT = {'"','(','['}

GOOD_ENDING_PUNCT = {',',';',':',')',']','"'}

STRONG_BIGRAMS = [
    ('ci', 'sono'),
    ('tanto', 'meglio'),
]

small_penalty = 5 # to avoid splitting too much
big_penalty = 50
medium_penalty = 25
no_penalty = 0 # to encourage splitting


class Penalizer_basic():

    VERSION = 'SPLIT_base_v1.0'
    
    def get_sequence_penalties(self, tokens):
        '''
        Given a list of tokens it returns len(tokens) penalties, one per word-boundary.
        e.g.    ['Nel', 'mondo', 'ci', 'sono', '22', 'milioni', 'di', 'consumatori', 'di', 'cocaina.']
        returns [     50,      5,    50,     5,    5,         5,    50,            5     50          0]
        '''    
        penalties = []
        for i, t in enumerate(tokens[:-1]):        
            tok = t.lower()
            tok_next = tokens[i+1].lower()
            penalties.append(self.get_boundary_penalty(tok, tok_next))    
        penalties.append(no_penalty)
        return penalties

    def get_boundary_penalty(self, tok, tok_next):    
        if tok in NOT_LONELEY:
            return big_penalty
        if any(tok.endswith(x) for x in GOOD_ENDING_PUNCT):
            return no_penalty
        if any(tok_next.startswith(x) for x in GOOD_BEGINNING_PUNCT):
            return no_penalty
        if tok_next in GOOD_BEGINNING_WORD:
            return no_penalty
        if (tok, tok_next) in STRONG_BIGRAMS:
            return big_penalty
        return small_penalty

    def get_line_length_penalty(self, lenght):
        if lenght == 1:
            return big_penalty
        if lenght == 2:
            return medium_penalty
        if lenght == 3:
            return small_penalty
        return no_penalty

if __name__ == "__main__":
    from subsegmenter import tokenizer
    BP = Penalizer_basic()
    sentence = "Nel mondo ci sono 22 milioni di consumatori di cocaina."
    tokens = tokenizer.tokenize_text(sentence)
    print(tokens)
    '''
    tokens = ['Nel', 'mondo', 'ci', 'sono', '22', 'milioni', 'di', 'consumatori', 'di', 'cocaina.']
    returns [     50,      5,    50,     5,     5,         5,    50,            5     50          0]
    '''
    print(BP.get_sequence_penalties(tokens))