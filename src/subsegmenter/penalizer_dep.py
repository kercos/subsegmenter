import spacy
from spacy import displacy

nlp = spacy.load('it_core_news_sm')

default_penalty = 5 # to avoid splitting too much
child_head_penalty = 50
commmon_head_penalty = 50
ancestor_penalty = 20
no_penalty = 0 # to encourage splitting
line_1_token_penalty = 100
line_2_token_penalty = 35
line_3_token_penalty = 15


class Penalizer_dep():

    VERSION = 'SPLIT_dep_v1.0'
    
    def get_sequence_penalties(self, str_tokens):
        '''
        Given a list of tokens it returns len(tokens) penalties, one per word-boundary.
        e.g.    ['Nel', 'mondo', 'ci', 'sono', '22', 'milioni', 'di', 'consumatori', 'di', 'cocaina']
        returns [     50,      5,    50,     20,   50,        20,   50             20    50        0]
        '''    
        sentence = ' '.join(str_tokens)
        doc = nlp(sentence)
        dep_tokens = list(doc)
        penalties = []
        for i, tok in enumerate(dep_tokens[:-1]):        
            penalties.append(self.get_boundary_penalty(tok, dep_tokens[i+1]))    
        if len(str_tokens)>0:
            penalties.append(no_penalty)
        self.adjust_tokenization_penalty(penalties,str_tokens,dep_tokens)
        return penalties

    def adjust_tokenization_penalty(self, penalties,str_tokens,dep_tokens):
        # adjusting tokenization splitting

        #print('\nSentence: {}'.format(' '.join(str_tokens)))
        adjusted_dep_token_list = []
        #print('old penalties: {}'.format(penalties))
        penalty_idx_to_remove = []
        i = 0
        for j in range(len(str_tokens)):   
            '''
            dep: 'Nel', 'mondo',  ',', 'ci', 'sono', '"',  '22', 'milioni', '"', 'di', 'consumatori', 'di', 'cocaina' '.'
            str: 'Nel', 'mondo,',      'ci', 'sono',      '"22', 'milioni"',     'di', 'consumatori', 'di', 'cocaina.'
            '''
            if dep_tokens[i].text != str_tokens[j]:
                #print('i={} j={} {} != {}'.format(i,j,dep_tokens[i].text,str_tokens[j]))
                adjusted_dep_tok = dep_tokens[i].text
                while adjusted_dep_tok != str_tokens[j]:
                    #print('\tdeleting index {}'.format(i))
                    penalty_idx_to_remove.append(i)
                    i+=1
                    adjusted_dep_tok += dep_tokens[i].text
                #print('\t{} == {}'.format(adjusted_dep_tok,str_tokens[j]))                
                adjusted_dep_token_list.append(adjusted_dep_tok)
                i+=1
            else:
                #print('i={} j={} {} == {}'.format(i,j,dep_tokens[i].text,str_tokens[j]))
                adjusted_dep_token_list.append(dep_tokens[i].text)
                i+=1
        #print('removing penalty indexes: {}'.format(penalty_idx_to_remove))
        for i in reversed(penalty_idx_to_remove):
            del penalties[i]
        #print('new penalties: {}'.format(penalties))
        sentence = ' '.join(str_tokens)
        assert(sentence == ' '.join(adjusted_dep_token_list))
        assert(len(penalties) == len(str_tokens))


    def get_boundary_penalty(self, tok, tok_next):    
        if tok.pos_ == 'PUNCT':
            return no_penalty
        if tok == tok_next.head or tok_next == tok.head:
            return child_head_penalty        
        if tok.head == tok_next.head:
            return commmon_head_penalty
        if tok in tok_next.ancestors or tok_next in tok.ancestors:
            return ancestor_penalty
        return default_penalty

    def get_line_length_penalty(self, lenght):
        if lenght == 1:
            return line_1_token_penalty
        if lenght == 2:
            return line_2_token_penalty
        if lenght == 3:
            return line_3_token_penalty
        return no_penalty

if __name__ == "__main__":
    from subsegmenter import tokenizer
    PD = Penalizer_dep()
    #sentence = 'Nel mondo ci sono 22 milioni di consumatori di cocaina.'
    sentence = "Un piacere che è tremendamente simile a quello del primo amore, e quindi un piacere che tutti ricordiamo e che tutti vorremmo riprovare [come] nel momento in cui lo abbiamo provato la prima volta."
    tokens = tokenizer.tokenize_text(sentence)
    print(tokens)
    '''
    tokens = ['Nel', 'mondo', 'ci', 'sono', '22', 'milioni', 'di', 'consumatori', 'di', 'cocaina.']
    returns [     50,      5,    50,     20,    50,        20,   50,            20    50        0]
    '''
    print(PD.get_sequence_penalties(tokens))
