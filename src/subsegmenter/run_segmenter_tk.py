import os
import sys
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from subsegmenter import segment_file, segment_dir
from subsegmenter import parameters

def main():
	root = tk.Tk()
	root.withdraw()

	def pick_a_file():
		file_path = filedialog.askopenfilename()
		if file_path is None or not os.path.isfile(file_path):
			print("Bye!")
			sys.exit()
		return file_path
		
	def pick_a_dir():
		dir_path = filedialog.askdirectory()
		if dir_path is None or not os.path.isdir(dir_path):
			print("Bye!")
			sys.exit()	
		return dir_path

	print("Do you want to select\n1) a file or 2) a directory?")
	choice = ''
	while choice not in ['1','2']:
		choice = input("Type 1 or 2: ")

	penalizer = parameters.DEFAULT_PENALIZER

	if choice=='1':
		file_path = pick_a_file()	
		print("Chosen file: {}".format(file_path))	
		success = segment_file(penalizer, file_path, output_writable_func=sys.stdout.write)
		if success:
			basename = os.path.basename(file_path)
			print("File '{}' segmented (see file in same dir ending with '{}.txt')".format(basename, penalizer.VERSION))
	else:
		dir_path = pick_a_dir()
		print("Chosen dir: {}".format(dir_path))	
		num_files = segment_dir(penalizer, dir_path, output_writable_func=sys.stdout.write)
		print("{} files segmented (see files in same dir ending with '{}.txt')".format(num_files, penalizer.VERSION))



if __name__ == '__main__':
	main()