import requests
from key import AMARA_HEADERS
import json

'''
https://apidocs.amara.org/

PROJECTS
64329	TOTAL
59762	tedxtalks
 3228	tedtalks
 1278	ted-ed
   36	otp-resources
   25	None
'''

# get the subtitles data
def get_subtitles(url, lang_code, pagination_data):

    # get the video ids checking the language and the published status
    selected_video_info = {} 
    for video_info in pagination_data['objects']:
        for lang_info in video_info['languages']:
            if (lang_info['code']) == lang_code and (lang_info['published']) == True:                
                selected_video_info[video_info['id']] = {
                    'project': video_info['project'],
                    'subtitles_uri': lang_info['subtitles_uri']
                }
                continue

    # get the subtitles
    for video_id, video_info in selected_video_info.items():
        project = video_info['project']
        sub_url = video_info['subtitles_uri']        
        response = requests.get(sub_url, headers=AMARA_HEADERS)
        sub_json = response.json()
        
        output_file = '../subtitles/amara/json/{}/{}_{}.json'.format(lang_code, project, video_id)
        with open(output_file, 'w') as f_out:
            json.dump(sub_json, f_out, indent=3, ensure_ascii=False)
        print(output_file)


def run_script(lang_code):
    i = 0
    page_size = 100
    initial_offset = 0 # 57900 # to re-start if interrupted
    while(True):

        offset = initial_offset + page_size * i
        url = 'https://amara.org/api/videos/'
        parameters = {
            'team': 'ted',
            # 'project': '', # tedtalks, tedxtalks
            'offset': offset,
            'limit': page_size,
            'primary_audio_language_code': lang_code
        }        

        response = requests.get(url, headers=AMARA_HEADERS, params=parameters)
        pagination_data = response.json()
        meta_data = pagination_data['meta']
        total_count = meta_data['total_count']
        print('{}-{}/{}'.format(offset+1, offset+page_size, total_count))

        get_subtitles(url, lang_code, pagination_data)
        
        if meta_data['next'] is None:
            break

        i += 1


if __name__ == "__main__":
    run_script('en')
