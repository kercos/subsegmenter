import os
import sys
from subsegmenter import segment_file, segment_dir
from subsegmenter import parameters

def main():

	def pick_a_file():
		file_path = input('Input the path of a file to segment: ')				
		if not os.path.isfile(file_path):
			print("Bye!")
			sys.exit()
		return file_path
		
	def pick_a_dir():
		dir_path = input('Input the path of a dir with files to segment: ')				
		if not os.path.isdir(dir_path):
			print("Bye!")
			sys.exit()	
		return dir_path

	print("Do you want to select\n1) a file or 2) a directory?")
	choice = ''
	while choice not in ['1','2']:
		choice = input("Type 1 or 2: ")

	penalizer = parameters.DEFAULT_PENALIZER

	if choice=='1':
		file_path = pick_a_file()	
		print("Chosen file: {}".format(file_path))	
		success = segment_file(penalizer, file_path, output_writable_func=sys.stdout.write)
		if success:
			basename = os.path.basename(file_path)
			print("File '{}' segmented (see file in same dir ending with '{}.txt')".format(basename, penalizer.VERSION))
	else:
		dir_path = pick_a_dir()
		print("Chosen dir: {}".format(dir_path))	
		total, segmented = segment_dir(penalizer, dir_path, output_writable_func=sys.stdout.write)
		print("{}/{} files segmented (see files in same dir ending with '{}.txt')".format(segmented, total, penalizer.VERSION))

if __name__ == '__main__':
	main()