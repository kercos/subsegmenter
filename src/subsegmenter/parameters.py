from subsegmenter.penalizer_dep import Penalizer_dep    
DEFAULT_PENALIZER = Penalizer_dep()

MAX_CHAR_FOR_TEXT_SEGMENTATION = 2000
MAX_CHARS_PER_LINE = 42
MAX_LINES_PER_SUBTITLES = 2
MAX_CHARS_PER_SECOND = 21

