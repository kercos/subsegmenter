Automatically segmentation of subtitles
--

Federico Sangati - federico.sangati@gmail.com

There are two submodules: 
- subsegmenter: main algorithms for the segmentation
- bot: code for the telegram bot (see [@SubSegmenterBot](https://t.me/SubSegmenterBot))

Bot installation
- make a copy of `bot/key_tmp.py` into `bot/key.py` filling in the info
- to setup the webhook run `python -m bot.set_webhook`